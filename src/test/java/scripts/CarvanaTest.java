package scripts;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.Waiter;

public class CarvanaTest extends Base {

    @Test(priority = 1, description = "Validate Carvana home page title and url")
    public void titleValidation() {
        driver.get("https://www.carvana.com/");
        Assert.assertEquals(driver.getTitle(), "Carvana | Buy & Finance Used Cars Online | At Home Delivery");
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/");

    }

    @Test(priority = 2, description = "Validate the Carvana logo")
    public void logoValidation() {
        driver.get("https://www.carvana.com/");
        Assert.assertTrue(carvanaHomePage.logo.isDisplayed());

    }

    @Test(priority = 3, description = "Validate the main navigation section items")
    public void navigationValidation() {
        driver.get("https://www.carvana.com/");
        Waiter.pause(5);
        String[] expected = {"HOW IT WORKS", "ABOUT CARVANA", "SUPPORT & CONTACT"};
        int index = 0;
        for (WebElement element : carvanaHomePage.navigationList) {
            Assert.assertTrue(element.isDisplayed());
            Assert.assertEquals(element.getText(), expected[index++]);
        }
    }


    @Test(priority = 4, description = "Validate the sign in error message")
    public void signInErrorValidation() {
        driver.get("https://www.carvana.com/");
        carvanaHomePage.signInButton.click();
        carvanaHomePage.nameInput.sendKeys("johndoe@gmail.com");
        carvanaHomePage.passwordInput.sendKeys("abcd1234");
        carvanaHomePage.secondSignInButton.click();
        Assert.assertEquals(carvanaHomePage.errorMessage.getText(),
                "Email address and/or password combination is incorrect\n" +
                        "Please try again or reset your password.");

    }

    @Test(priority = 5, description = "Validate the search filter options and search button")
    public void searchFilterAndButtonValidation() {
        driver.get("https://www.carvana.com/");
        Waiter.pause(5);
        carvanaHomePage.searchCarsButton.click();
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/cars");
        String[] expected = {"PAYMENT & PRICE", "MAKE & MODEL", "BODY TYPE", "YEAR & MILEAGE", "FEATURES", "MORE FILTERS"};
        int index = 0;
        for (WebElement element : carvanaCarsPage.filterList) {
            Assert.assertTrue(element.isDisplayed());
            Assert.assertEquals(element.getText(), expected[index++]);

        }

    }

    @Test(priority = 6, description = "Validate the search result tiles")
    public void searchResultValidation() {
        driver.get("https://www.carvana.com/");
        Waiter.pause(5);
        carvanaHomePage.searchCarsButton.click();
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/cars");
        carvanaCarsPage.inputSearch.sendKeys("mercedes-benz");
        Waiter.pause(5);
        carvanaCarsPage.goButton.click();
        Waiter.pause(5);
        Assert.assertTrue(driver.getCurrentUrl().contains("mercedes-benz"));
        carvanaMercedesPage.popUpCloseButton.click();
        for (int i = 0; i < 19; i++) {
            Assert.assertTrue(carvanaMercedesPage.image.get(i).isDisplayed());
            Assert.assertTrue(carvanaMercedesPage.tile.get(i).isDisplayed());
            Assert.assertTrue(carvanaMercedesPage.favorite.get(i).isDisplayed());
            Assert.assertTrue(carvanaMercedesPage.inventory.get(i).isDisplayed());
            Assert.assertFalse(carvanaMercedesPage.image.get(i) == null);
            Assert.assertTrue(carvanaMercedesPage.makeModel.get(i).isDisplayed());
            Assert.assertFalse(carvanaMercedesPage.makeModel.get(i)==null);
            Assert.assertTrue(carvanaMercedesPage.mileage.get(i).isDisplayed());
            Assert.assertFalse(carvanaMercedesPage.mileage.get(i)==null);
            Assert.assertTrue(carvanaMercedesPage.price.get(i).isDisplayed());
            Assert.assertNotEquals(carvanaMercedesPage.price.get(i).getText(), 0 + "");
            Assert.assertTrue(carvanaMercedesPage.monthlyPayment.get(i).isDisplayed());
            Assert.assertFalse(carvanaMercedesPage.monthlyPayment.get(i)==null);
            Assert.assertTrue(carvanaMercedesPage.downPayment.get(i).isDisplayed());
            Assert.assertFalse(carvanaMercedesPage.downPayment.get(i)==null);
            Assert.assertTrue(carvanaMercedesPage.delivery.get(i).isDisplayed());
        }
    }

}
