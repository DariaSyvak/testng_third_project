package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.util.List;

public class CarvanaHomePage {
    public CarvanaHomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@data-qa='logo-wrapper']")
    public WebElement logo;

    @FindBy(xpath = "//div[@data-qa='navigation-wrapper']/div[@data-qa='menu-wrapper']")
    public List<WebElement> navigationList;

    @FindBy(xpath = "//div[@data-qa='sign-in-wrapper']")
    public WebElement signInButton;

    @FindBy(xpath = "//div[@data-cv-test='Header.Modal']")
    public WebElement modalIcon;

    @FindBy(id="usernameField")
    public WebElement nameInput;

    @FindBy(id="passwordField")
    public WebElement passwordInput;

    @FindBy(xpath = "(//button[@data-qa='button-base'])[1]")
    public WebElement secondSignInButton;

    @FindBy(xpath = "//div[@data-qa='error-message-container']")
    public WebElement errorMessage;

    @FindBy(xpath = "//a[@data-cv-test='headerSearchLink']")
    public WebElement searchCarsButton;





}
