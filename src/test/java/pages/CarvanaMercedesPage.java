package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CarvanaMercedesPage {
    public CarvanaMercedesPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//picture[@class='vehicle-image']")
    public List<WebElement> image;

    @FindBy(xpath ="//div[@class='favorite-vehicle']")
    public List<WebElement> favorite;

    @FindBy(xpath = "//div[@class='result-tile']")
    public List<WebElement> tile;

    @FindBy(xpath = "//div[@class='base-delivery-chip delivery-chip']")
    public List<WebElement> delivery;

    @FindBy(xpath = "//div[@data-test='InventoryTypeExperiment']")
    public List<WebElement> inventory;

    @FindBy(xpath = "//div[@class='make-model']")
    public List<WebElement> makeModel;

    @FindBy(xpath = "//div[@class='trim-mileage']")
    public List<WebElement> mileage;

    @FindBy(xpath = "//div[@class='price ']")
    public List<WebElement> price;

    @FindBy(xpath ="//div[@class='monthly-payment']" )
    public List<WebElement> monthlyPayment;

    @FindBy(xpath ="//div[@class='down-payment']" )
    public List<WebElement> downPayment ;

    @FindBy(css = ".close svg")
    public WebElement popUpCloseButton;






}
