package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CarvanaCarsPage {
    public CarvanaCarsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@data-qa='menu-flex']//button")
    public List<WebElement> filterList;

    @FindBy(xpath = "//input[@type='text']")
    public WebElement inputSearch;

    @FindBy(xpath = "//button[@data-qa='go-button']")
    public WebElement goButton;

}
